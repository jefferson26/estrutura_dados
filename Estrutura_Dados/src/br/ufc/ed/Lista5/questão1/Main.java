package br.ufc.ed.Lista5.quest�o1;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Tamanho Heap");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();
		
		//Item 1
		TAD_Heap heap = new TAD_Heap();	
		heap.cria_heap(n);
		
		//Item 2
		for(int i =0; i < n; i++)
			heap.insere_heap(i);
		
		heap.imprime_heap();
		
		//Item 3
		int nova = 0;
		nova = heap.busca_heap(5);
		System.out.println(heap.getValorByID(nova));
		System.out.println();
	
		//Item 4
		heap.remove_heap();
		heap.remove_heap();
		heap.imprime_heap();
		
		//Item 5
		heap.edita_heap(4, 27);
		heap.imprime_heap();
		
		//Item 6
		heap.libera_heap();
		
	}
}
