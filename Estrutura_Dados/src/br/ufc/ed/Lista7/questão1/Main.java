package br.ufc.ed.Lista7.quest�o1;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Tamanho Conjunto");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();		
		
		//Item 1
		TAD_Conj_Dijuntos conjunto = new TAD_Conj_Dijuntos();	
		conjunto.cria_conj(n);
		
		Celula[] celulas = new Celula[n];
		
		for(int i =0; i < n; i++){
			celulas[i] = new Celula();
			celulas[i].setValor(i);
		}
		
		conjunto.setValores(celulas);
		conjunto.makeSet();
		
		//Item 2
		conjunto.union(0, 1);
		conjunto.union(0, 2);
		conjunto.union(0, 3);
		conjunto.union(6, 7);
		conjunto.union(7, 8);
		conjunto.union(4, 5);
		conjunto.union(8, 9);
		
		//Item 3
		System.out.println(conjunto.find(0));
		System.out.println(conjunto.find(1));
		System.out.println(conjunto.find(2));
		System.out.println(conjunto.find(3));
		System.out.println(conjunto.find(4));
		System.out.println(conjunto.find(5));
		System.out.println(conjunto.find(6));
		System.out.println(conjunto.find(7));
		System.out.println(conjunto.find(8));
		System.out.println(conjunto.find(9));
		
		//Item 4
		if(!conjunto.mesmo_conjunto(2, 9))
			System.out.println("Cojuntos diferentes");
		
		if(conjunto.mesmo_conjunto(0, 1))
			System.out.println("Mesmo conjunro");
		
		//Item 5
		conjunto.liberar_conjunto();
		
	}
}
