package br.ufc.ed.Lista1.questão6;

public class ContaPoupança implements IContaBanco{

	private double saldo;
	private int numero;
	
	public void realizar_crédito_conta(double credito, IContaBanco conta) {
		ContaPoupança conta_pou = (ContaPoupança) conta;
		conta_pou.setSaldo(conta_pou.getSaldo() + credito);
	}
	
	public void realizar_debito_conta(double debito, IContaBanco conta) {
		ContaPoupança conta_pou = (ContaPoupança) conta;
		conta_pou.setSaldo(conta_pou.getSaldo() - debito);
	}

	public void render_juros(ContaPoupança conta_pou, double juros) {
		double adicional = (conta_pou.getSaldo()*(juros));
		
		conta_pou.setSaldo(conta_pou.getSaldo() + adicional);
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	
}
