package br.ufc.ed.Lista1.quest�o6;

public class ListaConta {
	
	private IContaBanco conta;
	private ListaConta proximo;
	
	public ListaConta cria_lst() {	
		ListaConta lst = new ListaConta();
		return lst;
	}

	public ListaConta insere_lst(ListaConta lst, IContaBanco conta) {
		ListaConta nova = new ListaConta();
		ListaConta ant = null;	
		ListaConta prox = lst.getProximo(); 
		
		while (prox != null && prox.getConta().getNumero() < conta.getNumero()) {
			ant = prox;
			prox = prox.getProximo();
		}
		nova.setConta(conta);
		
		if(ant == null){
			nova.setProximo(lst.getProximo());
			lst.setProximo(nova);
	
		}
		else {
			nova.setProximo(ant.getProximo());
			ant.setProximo(nova);
		}

		return lst;		
	}

	public void imprime_lst(ListaConta lst) {
		ListaConta prox = lst.getProximo();
		while(prox != null){
			System.out.println("N�mero conta = " + prox.getConta().getNumero());
			System.out.println("Saldo conta = " + prox.getConta().getSaldo());
			prox = prox.getProximo();
		}	
	}

	public void imprime_lst_rec(ListaConta lst) {
		if(lst.getProximo() != null){
			System.out.println("N�mero conta = " + lst.getProximo().getConta().getNumero());
			System.out.println("Saldo conta = " + lst.getProximo().getConta().getSaldo());
			imprime_lst_rec(lst.getProximo());
		}
	}

	public void imprime_lst_rev(ListaConta lst) {
		if(lst.getProximo() != null){
			imprime_lst_rev(lst.getProximo());
			System.out.println("N�mero conta = " + lst.getProximo().getConta().getNumero());
			System.out.println("Saldo conta = " + lst.getProximo().getConta().getSaldo());
		}
	}

	public boolean lst_vazia(ListaConta lst) {
		return (lst == null);
	}

	public ListaConta busca_lst(ListaConta lst, IContaBanco conta) {	
		ListaConta prox = lst.getProximo(); 
		
		while (prox != null) {
			if (prox.getConta().getNumero() == conta.getNumero())
				return prox;
			prox = prox.getProximo();
		}
		return null;
	}

	public void remove_lst(ListaConta lst, IContaBanco conta) {
		ListaConta ant = null;
		
		if(busca_lst(lst, conta) != null){
			lst = lst.getProximo();
			while (lst.getProximo() != null && lst.getConta().getNumero() < conta.getNumero()) {
				ant = lst;
				lst = lst.getProximo();
			}
			
			if(ant == null)
				this.proximo = lst.getProximo();
			
			else {
				ant.setProximo(lst.getProximo());
				lst = ant;
			}
		}
	}

	public ListaConta remove_lst_rec(ListaConta lst, IContaBanco conta) {
		if (!lst_vazia(lst)) {		
			if(lst.getConta() == null)
				lst.setProximo(remove_lst_rec(lst.getProximo(), conta));
			else if (lst.getConta().getNumero() == conta.getNumero()) 
				lst = lst.getProximo();
			else 
				lst.setProximo(remove_lst_rec(lst.getProximo(), conta));
		}
		return lst;
	}

	public ListaConta libera_lst(ListaConta lst) {
		ListaConta libera;
		while(lst != null){
			libera = lst.getProximo();
			lst = null;
			lst = libera; 
		}	
		
		return lst;
	}
	
	public void realizar_tranferencia(IContaBanco origem,  IContaBanco destino, double valor) {
		if(valor > origem.getSaldo())
			System.out.println("Saldo insuficiente");
		else{
			origem.realizar_debito_conta(valor, origem);
			destino.realizar_cr�dito_conta(valor, destino);
		}
	}
	public IContaBanco getConta() {
		return conta;
	}
	public void setConta(IContaBanco conta) {
		this.conta = conta;
	}
	public ListaConta getProximo() {
		return proximo;
	}
	public void setProximo(ListaConta proximo) {
		this.proximo = proximo;
	}
}
