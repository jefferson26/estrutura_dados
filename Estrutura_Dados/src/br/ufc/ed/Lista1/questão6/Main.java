package br.ufc.ed.Lista1.quest�o6;

public class Main {
	
	public static void main(String[] args) {
		
		
		Object obj = new ListaConta();
		ListaConta lst = null;	
		lst = ((ListaConta) obj).cria_lst();
		
		ContaNormal conta_nor = new ContaNormal();
		ContaPoupan�a conta_pou = new ContaPoupan�a ();
		ContaFidelidade conta_fid = new ContaFidelidade(); 
		
		conta_nor.setNumero(1);
		conta_nor.setSaldo(500.00);
		conta_pou.setNumero(2);
		conta_pou.setSaldo(1000.00);
		conta_fid.setNumero(3);
		conta_fid.setSaldo(1500.00);
		
		//Item1
		lst = lst.insere_lst(lst, conta_nor);
		//Item2
		lst = lst.insere_lst(lst, conta_pou);
		//Item3
		lst = lst.insere_lst(lst, conta_fid);
		
		//Item4
		conta_nor.realizar_cr�dito_conta(100.00, conta_nor);
		System.out.println("\nSaldo: " + conta_nor.getSaldo());
		
		//Item5
		conta_nor.realizar_debito_conta(53.02, conta_nor);
		//Item6
		System.out.println("Saldo: " + conta_nor.getSaldo());
		
		//Item7
		conta_fid.realizar_cr�dito_conta(1000.00, conta_fid);
		System.out.println("\nSaldo: " + conta_fid.getSaldo());
		System.out.println("Bonus: " + conta_fid.getBonus());
		
		//Item8
		lst.realizar_tranferencia(conta_fid, conta_nor, 430.52);
		System.out.println("\nSaldo: " + conta_fid.getSaldo());
		System.out.println("Saldo: " + conta_nor.getSaldo());
		
		//Item9
		conta_pou.render_juros(conta_pou, 0.01);
		System.out.println("\nSaldo: " + conta_pou.getSaldo());
		
		//Item10
		conta_fid.render_bonus(conta_fid);
		conta_fid.render_bonus(conta_fid);
		System.out.println("\nSaldo: " + conta_fid.getSaldo());
		System.out.println();
		
		//Item11
		lst.remove_lst(lst, conta_pou);
		lst.remove_lst_rec(lst, conta_nor);
		//Item12
		lst.imprime_lst(lst);
		
		
		System.out.println();
		lst.insere_lst(lst, conta_pou);
		lst.insere_lst(lst, conta_nor);
		lst.imprime_lst(lst);
	}
}
