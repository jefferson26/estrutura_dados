package br.ufc.ed.Lista1.quest�o6;

public interface IContaBanco {
	/*IContaBanco cria_conta();
	IContaBanco insere_conta(IContaBanco conta, int val);
	void imprime_conta(IContaBanco conta);
	void imprime_conta_rec(IContaBanco conta);
	void imprime_conta_rev(IContaBanco conta);
	boolean conta_vazia(IContaBanco conta);
	IContaBanco busca_conta(IContaBanco conta, int val);
	void remove_conta(IContaBanco conta, int val);
	IContaBanco remove_conta_rec(IContaBanco conta, int val);
	IContaBanco libera_conta(IContaBanco conta);
	*/
	void realizar_cr�dito_conta(double credito, IContaBanco conta);
	void realizar_debito_conta(double debito, IContaBanco conta);
	double getSaldo();
	void setSaldo(double saldo);
	public int getNumero();
	public void setNumero(int numero);
	//int consultar_saldo_conta(IContaBanco conta);
	//boolean realizar_transferencia_conta(IContaBanco origem, IContaBanco destino);
}
