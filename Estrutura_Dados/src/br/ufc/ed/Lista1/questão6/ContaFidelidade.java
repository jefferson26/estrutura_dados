package br.ufc.ed.Lista1.quest�o6;

public class ContaFidelidade implements IContaBanco{

	private double saldo;
	private int numero;
	private double bonus;

	public void realizar_cr�dito_conta(double credito, IContaBanco conta) {
		ContaFidelidade conta_fid = (ContaFidelidade) conta;
		conta_fid.setSaldo(conta_fid.getSaldo() + credito);
		
		conta_fid.setBonus(conta_fid.getBonus() + (0.01 * credito));	
	}

	public void realizar_debito_conta(double debito, IContaBanco conta) {
		ContaFidelidade conta_fid = (ContaFidelidade) conta;
		conta_fid.setSaldo(conta_fid.getSaldo() - debito);
	}
	
	public void render_bonus(ContaFidelidade conta) {
		conta.setSaldo(conta.getSaldo() + conta.getBonus());
		conta.setBonus(0);
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public double getBonus() {
		return bonus;
	}

	public void setBonus(double bonus) {
		this.bonus = bonus;
	}
}
