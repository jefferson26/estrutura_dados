package br.ufc.ed.Lista1.quest�o6;

public class ContaNormal implements IContaBanco{

	private double saldo;
	private int numero;
	
	public void realizar_cr�dito_conta(double credito, IContaBanco conta) {
		ContaNormal conta_nor = (ContaNormal) conta;
		conta_nor.setSaldo(conta_nor.getSaldo() + credito);
	}
	
	public void realizar_debito_conta(double debito, IContaBanco conta) {
		ContaNormal conta_nor = (ContaNormal) conta;
		conta_nor.setSaldo(conta_nor.getSaldo() - debito);
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}
}
