package br.ufc.ed.Lista1.quest�o3;

public class Main {
	
	public static void main(String[] args) {
		//Item 1
		Object obj = new TAD_Lista();
		TAD_Lista lst = null;	
		lst = ((TAD_Lista) obj).cria_lst();
		
		//Item 2
		lst = lst.insere_lst(lst, 7);
		lst = lst.insere_lst(lst, 12);
		lst = lst.insere_lst(lst, 2);
		lst = lst.insere_lst(lst, 8);
		lst = lst.insere_lst(lst, 1);
		lst = lst.insere_lst(lst, 4);
		lst = lst.insere_lst(lst, 5);
		
		//Item 3
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 4
		lst.imprime_lst_rec(lst);
		System.out.println("\n");
		
		//Item 5
		lst.imprime_lst_rev(lst);
		System.out.println("\n");
		
		//Item6
		if(!lst.lst_vazia(lst)){
			System.out.println("Lista n�o vazia");
		}
		
		//Item 7
		TAD_Lista nova = new TAD_Lista();
		nova = lst.busca_lst(lst, 4);
		System.out.println(nova.getInfo());
		
		nova = lst.busca_lst(lst, 3);
		if(nova == null){
			System.out.println("Elemento n�o encontrado");
		}
		
		//Item 8
		lst.remove_lst(lst, 6);
		lst.remove_lst(lst, 4);
		lst.remove_lst(lst, 1);
		lst.remove_lst(lst, 2);
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 9
		lst = lst.remove_lst_rec(lst, 7);
		lst = lst.remove_lst_rec(lst, 12);
		lst = lst.remove_lst_rec(lst, 5);
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 10
		lst = lst.libera_lst(lst);
		
		
		TAD_Lista vazio = new TAD_Lista();
		if(vazio.lst_vazia(vazio)){
			System.out.println("Lista vazia");
		}
		
		//Item 11
		TAD_Lista lst1 = new TAD_Lista();
		TAD_Lista lst2 = new TAD_Lista();
		lst1 = lst1.insere_lst(lst1, 7);
		lst1 = lst1.insere_lst(lst1, 12);
		lst1 = lst1.insere_lst(lst1, 2);
		lst2 = lst2.insere_lst(lst2, 7);
		lst2 = lst2.insere_lst(lst2, 12);
		lst2 = lst2.insere_lst(lst2, 2);
		if(lst1.lst_iguais(lst1, lst2))
			System.out.println("Listas Iguais");
		else
			System.out.println("Listas n�o Iguais");

		
	}
}
