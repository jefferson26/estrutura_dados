package br.ufc.ed.Lista1.quest�o3;

public interface TAD_ILista {
	TAD_Lista cria_lst();
	TAD_Lista insere_lst(TAD_Lista lst, int val);
	void imprime_lst(TAD_Lista lst);
	void imprime_lst_rec(TAD_Lista lst);
	void imprime_lst_rev(TAD_Lista lst);
	boolean lst_vazia(TAD_Lista lst);
	TAD_Lista busca_lst(TAD_Lista lst, int val);
	void remove_lst(TAD_Lista lst, int val);
	TAD_Lista remove_lst_rec(TAD_Lista lst, int val);
	TAD_Lista libera_lst(TAD_Lista lst);
}
