package br.ufc.ed.Lista1.quest�o5;

public class TAD_Lista_Circ implements TAD_ILista_Circ{
	

	private int info;
	private TAD_Lista_Circ proximo;
	private TAD_Lista_Circ anterior;

	public TAD_Lista_Circ cria_lst_circ() {	
		TAD_Lista_Circ lst = new TAD_Lista_Circ();
		return lst;
	}

	public TAD_Lista_Circ insere_lst_circ(TAD_Lista_Circ lst, int val) {
		TAD_Lista_Circ nova = new TAD_Lista_Circ();
		nova.setInfo(val);
		
		if (lst.getProximo() == null){
			lst.setProximo(nova);
			nova.setProximo(lst);
			nova.setAnterior(lst);
			lst.setAnterior(nova);
		}
		else{
			nova.setProximo(lst.getProximo());
			nova.setAnterior(lst);
			lst.getProximo().setAnterior(nova);
			lst.setProximo(nova);
			
		}
		return lst;
	}

	public void imprime_lst_circ(TAD_Lista_Circ lst) {
		TAD_Lista_Circ prox = lst.getProximo();
		while(prox != lst){
			System.out.println("circular = " + prox.getInfo());
			prox = prox.getProximo();
		}	
	}

	public void imprime_lst_circ_rec(TAD_Lista_Circ lst) {
		if(lst.getProximo().getProximo() != this.proximo){
			System.out.println("rec circular = " + lst.getProximo().getInfo());
			imprime_lst_circ_rec(lst.getProximo());
		}
	}

	public boolean lst_circ_vazia(TAD_Lista_Circ lst) {
		return (lst.getProximo() == null);
	}

	public TAD_Lista_Circ busca_lst_circ(TAD_Lista_Circ lst, int val) {
		TAD_Lista_Circ prox = lst.getProximo();
		while(prox != lst){
			if (prox.getInfo() == val)
				return prox;
			prox = prox.getProximo();
		}
		return null;
	}

	public void remove_lst_circ(TAD_Lista_Circ lst, int val) {
		TAD_Lista_Circ ant = null;
		if(busca_lst_circ(lst, val) != null){
			while (lst != null && lst.getInfo() != val) {
				ant = lst;
				lst = lst.getProximo();
			}
			
			if(ant == null){
				lst.getProximo().setAnterior(lst.getAnterior());
				lst = lst.getProximo();		
			}
			else {
				lst.getProximo().setAnterior(ant);
				ant.setProximo(lst.getProximo());
				lst = ant;
			}
		}
	}

	public TAD_Lista_Circ remove_lst_circ_rec(TAD_Lista_Circ lst, int val) {
		if (!lst_circ_vazia(lst)) {			
			if (lst.getInfo() == val) {
				lst.getProximo().setAnterior(lst.getAnterior());
				lst = lst.getProximo();
			}
			else if(lst.getProximo().getProximo() != this.proximo)
				lst.setProximo(remove_lst_circ_rec(lst.getProximo(), val));
		}
		return lst;
	}

	public TAD_Lista_Circ libera_lst_circ(TAD_Lista_Circ lst) {
		TAD_Lista_Circ libera;
		while(lst != null){
			libera = lst.getProximo();
			lst.setProximo(null);
			lst = libera; 
		}	
		
		return lst;
	}

	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

	public TAD_Lista_Circ getProximo() {
		return proximo;
	}

	public void setProximo(TAD_Lista_Circ proximo) {
		this.proximo = proximo;
	}
	
	public TAD_Lista_Circ getAnterior() {
		return anterior;
	}

	public void setAnterior(TAD_Lista_Circ anterior) {
		this.anterior = anterior;
	}
}
