package br.ufc.ed.Lista1.quest�o5;

public interface TAD_ILista_Circ {
	TAD_Lista_Circ cria_lst_circ();
	TAD_Lista_Circ insere_lst_circ(TAD_Lista_Circ lst, int val);
	void imprime_lst_circ(TAD_Lista_Circ lst);
	void imprime_lst_circ_rec(TAD_Lista_Circ lst);
	boolean lst_circ_vazia(TAD_Lista_Circ lst);
	TAD_Lista_Circ busca_lst_circ(TAD_Lista_Circ lst, int val);
	void remove_lst_circ(TAD_Lista_Circ lst, int val);
	TAD_Lista_Circ remove_lst_circ_rec(TAD_Lista_Circ lst, int val);
	TAD_Lista_Circ libera_lst_circ(TAD_Lista_Circ lst);
}
