package br.ufc.ed.Lista1.quest�o4;

public class Main {
	
	public static void main(String[] args) {
		//Item 1
		Object obj = new TAD_Lista_Circ();
		TAD_Lista_Circ lst = null;	
		lst = ((TAD_Lista_Circ) obj).cria_lst_circ();
		
		//Item 2
		lst = lst.insere_lst_circ(lst, 14);
		lst = lst.insere_lst_circ(lst, 8);
		lst = lst.insere_lst_circ(lst, 6);
		lst = lst.insere_lst_circ(lst, 12);
		lst = lst.insere_lst_circ(lst, 4);
		lst = lst.insere_lst_circ(lst, 2);
		lst = lst.insere_lst_circ(lst, 10);
		
		//Item 3
		lst.imprime_lst_circ(lst);
		System.out.println("\n");
		
		//Item 4
		lst.imprime_lst_circ_rec(lst);
		System.out.println("\n");
		
		//Item5
		if(!lst.lst_circ_vazia(lst)){
			System.out.println("Lista n�o vazia");
		}
		
		//Item 6
		TAD_Lista_Circ nova = new TAD_Lista_Circ();
		nova = lst.busca_lst_circ(lst, 4);
		System.out.println(nova.getInfo());
		
		nova = lst.busca_lst_circ(lst, 3);
		if(nova == null){
			System.out.println("Elemento n�o encontrado");
		}
		
		//Item 7
		lst.remove_lst_circ(lst, 6);
		lst.remove_lst_circ(lst, 4);
		lst.remove_lst_circ(lst, 1);
		lst.remove_lst_circ(lst, 2);
		lst.remove_lst_circ(lst, 10);
		lst.imprime_lst_circ(lst);
		System.out.println("\n");
		
		//Item 8
		lst = lst.remove_lst_circ_rec(lst, 6);
		lst = lst.remove_lst_circ_rec(lst, 14);
		lst = lst.remove_lst_circ_rec(lst, 14);
		lst.imprime_lst_circ(lst);
		System.out.println("\n");
		
		//Item 9
		lst = lst.libera_lst_circ(lst);
		
		
		TAD_Lista_Circ vazio = new TAD_Lista_Circ();
		if(vazio.lst_circ_vazia(vazio)){
			System.out.println("Lista vazia");
		}
	}
}
