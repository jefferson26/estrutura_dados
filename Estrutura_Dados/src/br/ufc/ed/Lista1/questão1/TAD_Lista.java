package br.ufc.ed.Lista1.quest�o1;

public class TAD_Lista implements TAD_ILista{
	

	private int info;
	private TAD_Lista proximo;

	public TAD_Lista cria_lst() {	
		TAD_Lista lst = new TAD_Lista();
		return lst;
	}

	public TAD_Lista insere_lst(TAD_Lista lst, int val) {
		TAD_Lista nova = new TAD_Lista();
		this.info = val;
		nova.setProximo(lst); 
		
		return nova;
	}

	public void imprime_lst(TAD_Lista lst) {
		TAD_Lista prox = lst.getProximo();
		while(prox != null){
			System.out.println("normal = " + prox.getInfo());
			prox = prox.getProximo();
		}	
	}

	public void imprime_lst_rec(TAD_Lista lst) {
		if(lst.getProximo() != null){
			System.out.println("rec = " + lst.getProximo().getInfo());
			imprime_lst_rec(lst.getProximo());
		}
	}

	public void imprime_lst_rev(TAD_Lista lst) {
		if(lst.getProximo() != null){
			imprime_lst_rev(lst.getProximo());
			System.out.println("rev = " + lst.getProximo().getInfo());	
		}
	}

	public boolean lst_vazia(TAD_Lista lst) {
		return (lst.getProximo() == null);
	}

	public TAD_Lista busca_lst(TAD_Lista lst, int val) {
		TAD_Lista prox = lst;
		while(prox != null){
			if (prox.getInfo() == val)
				return prox;
			prox = prox.getProximo();
		}
		return null;
	}

	public void remove_lst(TAD_Lista lst, int val) {
		TAD_Lista ant = null;
		if(busca_lst(lst, val) != null){
			while (lst != null && lst.getInfo() != val) {
				ant = lst;
				lst = lst.getProximo();
			}
			
			if(ant == null){
				lst = lst.getProximo();
			}
			else {
				ant.setProximo(lst.getProximo());
				lst = ant;
			}
		}
	}

	public TAD_Lista remove_lst_rec(TAD_Lista lst, int val) {
		if (!lst_vazia(lst)) {			
			if (lst.getInfo() == val) 
				lst = lst.getProximo();
			else 
				lst.setProximo(remove_lst_rec(lst.getProximo(), val));
		}
		return lst;
	}

	public TAD_Lista libera_lst(TAD_Lista lst) {
		TAD_Lista libera;
		while(lst != null){
			libera = lst.getProximo();
			lst = null;
			lst = libera; 
		}	
		
		return lst;
	}

	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

	public TAD_Lista getProximo() {
		return proximo;
	}

	public void setProximo(TAD_Lista proximo) {
		this.proximo = proximo;
	}
	
}
