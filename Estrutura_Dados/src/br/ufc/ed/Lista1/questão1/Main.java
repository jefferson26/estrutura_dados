package br.ufc.ed.Lista1.quest�o1;

public class Main {
	
	public static void main(String[] args) {
		//Item 1
		Object obj = new TAD_Lista();
		TAD_Lista lst = null;	
		lst = ((TAD_Lista) obj).cria_lst();
		
		//Item 2
		lst = lst.insere_lst(lst, 14);
		lst = lst.insere_lst(lst, 8);
		lst = lst.insere_lst(lst, 6);
		lst = lst.insere_lst(lst, 12);
		lst = lst.insere_lst(lst, 4);
		lst = lst.insere_lst(lst, 2);
		lst = lst.insere_lst(lst, 10);
		//Item 3
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 4
		lst.imprime_lst_rec(lst);
		System.out.println("\n");
		
		//Item 5
		lst.imprime_lst_rev(lst);
		System.out.println("\n");
		
		//Item6
		if(!lst.lst_vazia(lst)){
			System.out.println("Lista n�o vazia");
		}
		
		//Item 7
		TAD_Lista nova = new TAD_Lista();
		nova = lst.busca_lst(lst, 4);
		System.out.println(nova.getInfo());
		
		nova = lst.busca_lst(lst, 3);
		if(nova == null){
			System.out.println("Elemento n�o encontrado");
		}
		
		//Item 8
		lst.remove_lst(lst, 6);
		lst.remove_lst(lst, 4);
		lst.remove_lst(lst, 1);
		lst.remove_lst(lst, 2);
		lst.remove_lst(lst, 10);
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 9
		lst = lst.remove_lst_rec(lst, 6);
		lst = lst.remove_lst_rec(lst, 14);
		lst.imprime_lst(lst);
		System.out.println("\n");
		
		//Item 10
		lst = lst.libera_lst(lst);
		
		
		TAD_Lista vazio = new TAD_Lista();
		if(vazio.lst_vazia(vazio)){
			System.out.println("Lista vazia");
		}
	}
}
