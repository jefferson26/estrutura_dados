package br.ufc.ed.Lista4.quest�o1;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Quantidade de chaves");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.println("Tamanho bucket");
		int tamanho = sc.nextInt();
		sc.close();
		
		//Item 1
		TAD_Hash hash = new TAD_Hash();	
		hash.cria_hash(n, tamanho);
		
		//Item 2
		for(int i =1; i <= n; i++)
			hash.insere_hash(i);
		
		hash.imprime_hash();
		
		//Item 3
		int nova = 0;
		nova = hash.busca_hash(5);
		System.out.println(nova);
	
		//Item 4
		hash.remove_hash(6);
		hash.remove_hash(7);
		hash.remove_hash(10);
		hash.imprime_hash();
		
		//Item 5
		hash.libera_hash();
	}
}
