package br.ufc.ed.Lista6.quest�o1;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Tamanho Conjunto");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();
		
		String valores[] = new String[n];	
		for(int i =0; i < n; i++){
			valores[i] = ""+i;
		}
		
		
		//Item 1
		TAD_Conjunto conjunto = new TAD_Conjunto();	
		conjunto.cria_conjunto(n, valores);
		
		//Item 2
		conjunto.insere_conjunto("0");
		conjunto.insere_conjunto("1");
		conjunto.insere_conjunto("2");
		conjunto.insere_conjunto("3");
		conjunto.imprimir_conjunto();
		
		//Item 3
		conjunto.remove_conjunto("2");
		conjunto.imprimir_conjunto();
		
		//Item 4
		TAD_Conjunto conjunto2 = new TAD_Conjunto();	
		conjunto2.cria_conjunto(n, valores);
		conjunto2.insere_conjunto("3");
		conjunto2.insere_conjunto("5");
		
		conjunto2.setBits(conjunto.uniao_conjuntos(conjunto2));
		
		conjunto2.imprimir_conjunto();
		
		//Item 5
		TAD_Conjunto conjunto3 = new TAD_Conjunto();	
		conjunto3.cria_conjunto(n, valores);
		conjunto3.insere_conjunto("2");
		conjunto3.insere_conjunto("1");
		
		conjunto3.setBits(conjunto.interse��o_conjuntos(conjunto3));
		conjunto3.imprimir_conjunto();
		
		//Item 6
		conjunto2.setBits(conjunto2.diferen�a_conjuntos(conjunto3));
		conjunto2.imprimir_conjunto();
		
		//Item 7
		conjunto.imprimir_conjunto();
		
		if(conjunto.verifica_subconjunto(conjunto2))
			System.out.println("Conjunto 2 � subconjunto de conjunto 1");
		else
			System.out.println("Conjunto 2 n�o � subconjunto de conjunto 1");
		
		//Item 8
		if (conjunto.verifica_conjuntos_iguais(conjunto2))
			System.out.println("Conjuntos iguais\n");
		else
			System.out.println("Conjuntos diferentes\n");
		
		//Item 9
		conjunto3.setBits(conjunto.complemento_conjunto());
		conjunto3.imprimir_conjunto();
		
		//Item 10
		if(conjunto.pertence_conjunto("1"))
			System.out.println("Pertence \n");
		else
			System.out.println("N�o Pertence \n");
		
		//Item 11
		System.out.println(conjunto.numero_elementos_conjunto());
		
		//Item 12
		conjunto.libera_conjunto();
		conjunto2.libera_conjunto();
	}
}
