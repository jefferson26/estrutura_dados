package br.ufc.ed.Lista6.quest�o1;

public interface TAD_IConjunto {
	void cria_conjunto(int n, String[] valores);
	void insere_conjunto(String val);
	void remove_conjunto(String val);
	int uniao_conjuntos(TAD_Conjunto conj);
	int interse��o_conjuntos(TAD_Conjunto conj);
	int diferen�a_conjuntos(TAD_Conjunto conj);
	boolean verifica_subconjunto(TAD_Conjunto conj);
	boolean verifica_conjuntos_iguais(TAD_Conjunto conj);
	int complemento_conjunto();
	boolean pertence_conjunto(String val);
	int numero_elementos_conjunto();
	void libera_conjunto();

}
