package br.ufc.ed.Lista3.quest�o1;

public interface TAD_IHash {
	void cria_hash(int n);
	void insere_hash(int val);
	void imprime_hash();
	Celula busca_hash(int val);
	void remove_hash(int val);
	void libera_hash();
}
