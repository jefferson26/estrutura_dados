package br.ufc.ed.Lista3.quest�o1;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		System.out.println("Quantidade de chaves");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();
		
		//Item 1
		TAD_Hash hash = new TAD_Hash();	
		hash.cria_hash(n);
		
		//Item 2
		for(int i =1; i <= n; i++)
			hash.insere_hash(i);
		
		hash.imprime_hash();
		
		//Item 3
		Celula nova = new Celula();
		nova = hash.busca_hash(5);
		System.out.println(nova.getInfo());
	
		//Item 4
		hash.remove_hash(6);
		hash.remove_hash(16);
		hash.remove_hash(16);
		hash.imprime_hash();
		
		//Item 5
		hash.libera_hash();
		
	}
}
