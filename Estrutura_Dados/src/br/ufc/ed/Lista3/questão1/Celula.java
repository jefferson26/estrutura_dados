package br.ufc.ed.Lista3.quest�o1;

public class Celula{
	

	private int info;
	private Celula proximo;

	public Celula cria_lst() {	
		Celula lst = new Celula();
		return lst;
	}

	public Celula insere_lst(Celula lst, int val) {
		Celula nova = new Celula();
		this.info = val;
		nova.setProximo(lst); 
		
		return nova;
	}

	public void imprime_lst(Celula lst) {
		Celula prox = lst.getProximo();
		System.out.print("Hash ");
		while(prox != null){
			System.out.print(prox.getInfo() + " -> ");
			prox = prox.getProximo();
		}	
		System.out.print("null\n");
	}

	public void imprime_lst_rec(Celula lst) {
		if(lst.getProximo() != null){
			System.out.println("rec = " + lst.getProximo().getInfo());
			imprime_lst_rec(lst.getProximo());
		}
	}

	public void imprime_lst_rev(Celula lst) {
		if(lst.getProximo() != null){
			imprime_lst_rev(lst.getProximo());
			System.out.println("rev = " + lst.getProximo().getInfo());	
		}
	}

	public boolean lst_vazia(Celula lst) {
		return (lst.getProximo() == null);
	}

	public Celula busca_lst(Celula lst, int val) {
		Celula prox = lst;
		while(prox != null){
			if (prox.getInfo() == val)
				return prox;
			prox = prox.getProximo();
		}
		return null;
	}

	public void remove_lst(Celula lst, int val) {
		Celula ant = null;
		if(busca_lst(lst, val) != null){
			while (lst != null && lst.getInfo() != val) {
				ant = lst;
				lst = lst.getProximo();
			}
			
			if(ant == null){
				lst = lst.getProximo();
			}
			else {
				ant.setProximo(lst.getProximo());
				lst = ant;
			}
		}
	}

	public Celula remove_lst_rec(Celula lst, int val) {
		if (!lst_vazia(lst)) {			
			if (lst.getInfo() == val) 
				lst = lst.getProximo();
			else 
				lst.setProximo(remove_lst_rec(lst.getProximo(), val));
		}
		return lst;
	}

	public Celula libera_lst(Celula lst) {
		Celula libera;
		while(lst != null){
			libera = lst.getProximo();
			lst = null;
			lst = libera; 
		}	
		
		return lst;
	}

	public int getInfo() {
		return info;
	}

	public void setInfo(int info) {
		this.info = info;
	}

	public Celula getProximo() {
		return proximo;
	}

	public void setProximo(Celula proximo) {
		this.proximo = proximo;
	}
	
}
